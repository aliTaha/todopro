import sys
from funs import sqlf
from sqlclass import sqlclass
from Form_MainWindow import Form_MainWindow

class App():
    def __init__(self):
        sqlf.conf = sqlclass('db-v2.db')
        self.MainWindow = Form_MainWindow()
        self.MainWindow.show()