
# python setup.py bdist_msi

from cx_Freeze import setup, Executable

setup(
    name="ToDoPro",
    version="2.0.0",
    author='Ali Taha',
    description='App Description',
    options={"build_exe": {
        'packages': ["os"],
        'include_files': [
            'icons/',
        ],
        'include_msvcr': True,
    }},
    executables=[Executable("AppMain.py", base="Win32GUI", icon="icons/icon.png")]
)