from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton, QWidget, QVBoxLayout, QLineEdit, QMessageBox, QDialog, QLabel, QTableWidget, \
    QHeaderView, QHBoxLayout, QGroupBox, QGridLayout

from funs import sqlf

class Form_Category(QDialog, QWidget):
    def __init__(self, parent=None):
        super(Form_Category, self).__init__(parent)
        self.setupUi(self)

    def setupUi(self, Dialog):
        self.title = 'Categories'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon("icons/icon.png"))

        self.layout = QVBoxLayout(self)
        Dialog.resize(340, 320)

        self.grid = QGridLayout()
        self.layout.addLayout(self.grid)

        self.setupUi_add()
        self.setupUi_edit()

        # Category Table
        self.MainTable = QTableWidget()
        self.MainTable.setShowGrid(True)
        self.MainTable.setColumnCount(2)
        self.MainTable.setHorizontalHeaderLabels(['Category', 'Actions'])
        self.MainTable.resize(240, 320)
        self.MainTable.verticalHeader().setDefaultSectionSize(36)

        box_categories = QHBoxLayout()
        box_categories.addWidget(self.MainTable)
        self.layout.addLayout(box_categories)

        header = self.MainTable.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)

        self.loadData()

    def setupUi_add(self):
        self.groupBox_add = QGroupBox("Add Category")
        self.grid.addWidget(self.groupBox_add, 0, 0)
        vbox = QVBoxLayout()
        self.groupBox_add.setLayout(vbox)

        label_category_name = QLabel()
        label_category_name.setText('Category Name:')
        vbox.addWidget(label_category_name)

        self.groupAdd_text_category_name = QLineEdit()
        vbox.addWidget(self.groupAdd_text_category_name)

        butt_addCategory = QPushButton('Add Category')
        butt_addCategory.clicked.connect(self.click_addCategory)
        vbox.addWidget(butt_addCategory)

    def setupUi_edit(self):
        self.groupBox_edit = QGroupBox("Edit Category")
        self.grid.addWidget(self.groupBox_edit, 0, 1)
        vbox = QVBoxLayout()
        self.groupBox_edit.setLayout(vbox)

        label_category_name = QLabel()
        label_category_name.setText('Category Name:')
        vbox.addWidget(label_category_name)

        self.groupEdit_text_category_name = QLineEdit()
        vbox.addWidget(self.groupEdit_text_category_name)

        butt_editCategory = QPushButton('Save')
        butt_editCategory.clicked.connect(self.click_editCategory)
        vbox.addWidget(butt_editCategory)

        butt_cansel = QPushButton('Cansel')
        butt_cansel.setMaximumWidth(70)
        butt_cansel.clicked.connect(self.click_butt_cansel)
        vbox.addWidget(butt_cansel)

        box_buttons = QHBoxLayout()
        box_buttons.addWidget(butt_editCategory)
        box_buttons.addWidget(butt_cansel)
        vbox.addLayout(box_buttons)

        self.groupBox_edit.setVisible(False)

    def click_butt_cansel(self):
        self.groupBox_edit.setVisible(False)
        self.groupBox_add.setVisible(True)

    def click_editCategory(self):

        category_id = self.edit_category_id
        category_name = self.groupEdit_text_category_name.text()
        if not category_name:
            return QMessageBox.about(self, "Alert", "Please enter the category name")

        category = sqlf.get_category_by_name(category_name)
        if category and category["category_id"] != category_id:
            return QMessageBox.about(self, "Error", "The category name already exists")

        status = sqlf.edit_category(category_id, category_name)
        if status > 0:
            self.groupBox_add.setVisible(True)
            self.groupBox_edit.setVisible(False)
            self.loadData()
        else:
            QMessageBox.about(self, "Error query", "An error in the process of input")

    def click_addCategory(self):
        category_name = self.groupAdd_text_category_name.text()
        if not category_name:
            return QMessageBox.about(self, "Alert", "Please enter the category name")

        category = sqlf.get_category_by_name(category_name)
        if category:
            return QMessageBox.about(self, "Error", "The category name already exists")

        status = sqlf.add_category(category_name)
        if status > 0:
            self.groupAdd_text_category_name.clear()
            self.loadData()
        else:
            QMessageBox.about(self, "Error query", "An error in the process of input")

    def loadData(self):
        # clear all data in table
        self.MainTable.setRowCount(0)

        results = sqlf.get_all_category()
        if results:
            for row in results:
                self.fetch_row(row)

    def fetch_row(self, row):
        rowPosition = self.MainTable.rowCount()
        self.MainTable.insertRow(rowPosition)

        class StyleTitle(QWidget):
            def __init__(self, parent=None):
                super(StyleTitle, self).__init__(parent)
                self.parent = parent

                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignCenter | Qt.AlignLeft)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                label = QLabel()
                label.setText(row["category_name"])
                layout.addWidget(label)

                self.setLayout(layout)

        self.MainTable.setCellWidget(rowPosition, 0, StyleTitle(self))

        class StyleButtonsActions(QWidget):

            def __init__(self, parent=None):
                super(StyleButtonsActions, self).__init__(parent)
                self.parent = parent
                # add your buttons
                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignCenter)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                ### button edit
                butt_edit = QPushButton()
                butt_edit.clicked.connect(self.click_butt_edit)
                butt_edit.setIcon(QIcon("icons/edit.png"))
                layout.addWidget(butt_edit)

                ### button delete
                butt_delete = QPushButton()
                # butt_delete.row = row
                butt_delete.clicked.connect(self.click_butt_delete)
                butt_delete.setIcon(QIcon("icons/delete.png"))
                layout.addWidget(butt_delete)

                self.setLayout(layout)

            def click_butt_edit(self):
                self.parent.groupBox_add.setVisible(False)
                self.parent.groupBox_edit.setVisible(True)

                self.parent.edit_category_id = row["category_id"]
                self.parent.groupEdit_text_category_name.setText(row["category_name"])

            def click_butt_delete(self):
                category_id = row["category_id"]
                category_name = row["category_name"]

                if sqlf.get_all_task_by_category_id(category_id):
                    return QMessageBox.about(self, "Warning", "The ({}) category cannot be deleted because it contains tasks, please delete the associated tasks first".format(category_name))

                msgBox = QMessageBox(self)
                reply = msgBox.question(self, 'Warning',("Are you sure to delete:({})".format(category_name)),
                                        QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    if sqlf.delete_category_by_id(category_id) < 1:
                        return QMessageBox.about(self, "Error query", "The category:({}) was not deleted".format(category_name))

                self.parent.loadData()

        self.MainTable.setCellWidget(rowPosition, 1, StyleButtonsActions(self))
