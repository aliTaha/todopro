from PyQt5.QtCore import Qt, QRect
from PyQt5.QtWidgets import QPushButton, QWidget, QTableWidget, QHeaderView, QHBoxLayout, QMessageBox, \
    QLabel, QTableView, QGridLayout, QDialog, QVBoxLayout, QGroupBox
from PyQt5.QtGui import QIcon

from Form_EditTask import Form_EditTask
from funs import sqlf

class TabTableTasks(QWidget):

    def showEvent(self, QShowEvent):
        self.relocadData()

    def __init__(self, parent, type):
        super().__init__()

        self.dataType = type
        self.MainTable = QTableWidget()
        self.MainTable.setShowGrid(True)
        self.MainTable.setColumnCount(4)
        self.MainTable.setHorizontalHeaderLabels(['Task Title', 'Category', 'Status', 'Actions'])

        self.MainTable.setSelectionBehavior(QTableView.SelectRows)
        self.MainTable.setStyleSheet("selection-background-color: antiquewhite;")

        self.MainTable.clicked.connect(self.viewClicked)

        header = self.MainTable.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(3, QHeaderView.ResizeToContents)

        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.MainTable)

    def viewClicked(self, clickedIndex):
        row = clickedIndex.row()
        # print(row)
        self.MainTable.verticalHeader().setDefaultSectionSize(37)
        # print(self.tableRows[row].setVisible(True))
        for trow in self.tableRows:
            self.tableRows[trow].setVisible(trow == row)

        self.MainTable.resizeRowToContents(row)


    def relocadData(self):
        self.tableRows = {}
        self.MainTable.setRowCount(0)
        self.getData()

    def getData(self):
        if self.dataType == "pending":
            resulrs = sqlf.get_all_task_pending()
        elif self.dataType == "completed":
            resulrs = sqlf.get_all_task_completed()
        elif self.dataType == "deferred":
            resulrs = sqlf.get_all_task_deferred()
        else:
            resulrs = sqlf.get_all_task()

        if resulrs:
            for row in resulrs:
                self.getData_fetch_row(row)

        self.MainTable.verticalHeader().setDefaultSectionSize(37) # Or self.MainTable.resizeRowsToContents()

    def getData_fetch_row(self, row):
        rowPosition = self.MainTable.rowCount()
        self.MainTable.insertRow(rowPosition)

        class StyleTitle(QDialog, QWidget):
            def __init__(self, parent=None):
                super(StyleTitle, self).__init__(parent)
                self.parent = parent

                layout = QVBoxLayout()
                layout.setAlignment(Qt.AlignTop)
                layout.setContentsMargins(5, 11, 5, 5)
                layout.setSpacing(0)

                grid = QVBoxLayout()
                layout.addLayout(grid)

                label_task_title = QLabel(row["task_title"])
                label_task_title.setMaximumHeight(15)
                # label_task_title.setWordWrap(True)
                grid.addWidget(label_task_title)

                tags = sqlf.get_tags_by_task_id(row["task_id"])
                if row["task_description"] or tags :
                    groupBox_detals = QGroupBox("")
                    grid.addWidget(groupBox_detals)

                    layout_groupBox = QVBoxLayout()
                    layout_groupBox.setContentsMargins(5, 0, 5, 5)
                    groupBox_detals.setLayout(layout_groupBox)

                    if row["task_description"] :
                        label_task_description = QLabel(row["task_description"])
                        label_task_description.setWordWrap(True)
                        layout_groupBox.addWidget(label_task_description)

                    if tags:
                        layout_tags = QHBoxLayout()
                        layout_tags.addStretch(1)
                        layout_tags.setContentsMargins(0, 0, 0, 0)
                        layout_groupBox.addLayout(layout_tags)
                        for tag in tags:
                            label_tag = QLabel(tag["tag_name"])
                            label_tag.setStyleSheet("background-color: #afd2f1;border-radius: 9px;padding: 5px;margin: 0px;")
                            label_tag.setScaledContents(False);
                            layout_tags.addWidget(label_tag)


                    groupBox_detals.setVisible(False)
                    self.parent.tableRows[rowPosition] = groupBox_detals

                self.setLayout(layout)

        self.MainTable.setCellWidget(rowPosition, 0, StyleTitle(self))

        class StyleCategory(QWidget):
            def __init__(self, parent=None):
                super(StyleCategory, self).__init__(parent)
                self.parent = parent
                # add your buttons
                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignTop)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                label = QLabel()
                label.setStyleSheet("background-color: #ffb84e;border-radius: 9px;padding: 5px;")
                label.setText(row["category_name"])
                label.setMaximumWidth(500)
                label.setWordWrap(True)
                layout.addWidget(label)

                self.setLayout(layout)

        if row["category_name"] :
            self.MainTable.setCellWidget(rowPosition, 1, StyleCategory(self))

        class StyleStatus(QWidget):
            def __init__(self, parent=None):
                super(StyleStatus, self).__init__(parent)
                self.parent = parent

                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignTop)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                label = QLabel()
                label.setStyleSheet("border-radius: 3px;padding: 5px;font-weight: bold;color: #fff;background-color: {};".format(self.color(row["task_status"])));
                label.setText(sqlf.convert_status(row["task_status"]))
                # label.setMaximumHeight(26)
                layout.addWidget(label)

                self.setLayout(layout)

            def color(self, status):
                if status == "p":
                    return "#007eff"
                if status == "c":
                    return "#0aa100"
                if status == "d":
                    return "#787878"
                return ""

        self.MainTable.setCellWidget(rowPosition, 2, StyleStatus(self))

        class StyleButtonsActions(QWidget):

            def __init__(self, parent=None):
                super(StyleButtonsActions, self).__init__(parent)
                self.parent = parent
                # add your buttons
                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignTop)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                ### button pending
                if row["task_status"] != "p":
                    butt_pending = QPushButton()
                    butt_pending.clicked.connect(self.click_butt_pending)
                    butt_pending.setIcon(QIcon("icons/pending.png"))
                    layout.addWidget(butt_pending)

                ### button completed
                if row["task_status"] != "c":
                    butt_completed = QPushButton()
                    butt_completed.clicked.connect(self.click_butt_completed)
                    butt_completed.setIcon(QIcon("icons/completed.png"))
                    layout.addWidget(butt_completed)

                ### button deferred
                if row["task_status"] != "d":
                    butt_deferred = QPushButton()
                    butt_deferred.clicked.connect(self.click_butt_deferred)
                    butt_deferred.setIcon(QIcon("icons/deferred.png"))
                    layout.addWidget(butt_deferred)

                ### button edit
                butt_edit = QPushButton()
                butt_edit.clicked.connect(self.click_butt_edit)
                butt_edit.setIcon(QIcon("icons/edit.png"))
                layout.addWidget(butt_edit)

                ### button delete
                butt_delete = QPushButton()
                # butt_delete.row = row
                butt_delete.clicked.connect(lambda: self.click_butt_delete(butt_delete))
                butt_delete.setIcon(QIcon("icons/delete.png"))
                layout.addWidget(butt_delete)

                self.setLayout(layout)

            def click_butt_edit(self):
                form_editTask = Form_EditTask(self, row)
                form_editTask.exec_()
                self.parent.relocadData()

            def click_butt_delete(self, butt):
                msgBox = QMessageBox(self)
                reply = msgBox.question(self, 'Warning', ("Are you sure to delete task ID:({})".format(row["task_id"])),
                                        QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    if sqlf.delete_task_by_id(row["task_id"]) < 1:
                        QMessageBox.about(self, "Error query", "The task ID:({}) was not deleted".format(row["task_id"]))
                    else:
                        self.parent.relocadData()

            def click_butt_pending(self):
                sqlf.edit_task_status(row["task_id"], "p")
                self.parent.relocadData()

            def click_butt_completed(self):
                sqlf.edit_task_status(row["task_id"], "c")
                self.parent.relocadData()

            def click_butt_deferred(self):
                sqlf.edit_task_status(row["task_id"], "d")
                self.parent.relocadData()

        self.MainTable.setCellWidget(rowPosition, 3, StyleButtonsActions(self))