import sqlite3

import os.path

class sqlclass():
    def __init__(self, db_file):

        self.create_connection(db_file)
        self.cur = self.conn.cursor()

    def create_connection(self, db_file):
        """ create a database connection to a SQLite database """
        self.conn = None
        self.cur = None

        try:
            if os.path.isfile(db_file):
                self.conn = sqlite3.connect(db_file)
                self.conn.row_factory = self.dict_factory
            else:
                self.conn = sqlite3.connect(db_file)
                self.conn.row_factory = self.dict_factory
                self.create_tables()
                if True:
                    self.demo()

            print("sqlite version", sqlite3.version)

        except sqlite3.Error as e:
            print("ERROR", e)

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def cur_query(self, q, *args):
        if not args or (len(args) == 1 and args[0] == None):
            return self.cur.execute(q)
        else:
            return self.cur.execute(q, args[0])

    def cur_fetchall(self, q, *args):
        self.cur_query(q, args)
        return self.cur.fetchall()

    def cur_fetchone(self, q, *args):
        self.cur_query(q, args)
        return self.cur.fetchone()

    def cur_insert(self, q, *args):
        re = self.cur_query(q, args)
        if re.rowcount > 0:
            self.commit()
            return self.cur.lastrowid

        return False

    def query(self, q, *args):
        if not args or (len(args) == 1 and args[0] == None):
            return self.conn.execute(q)
        else:
            return self.conn.execute(q, args[0])

    def commit(self):
        self.conn.commit()

    def execute(self, q, *args):
        re = self.query(q, args)
        self.commit()
        return re.rowcount

    def demo(self):
        self.execute("INSERT INTO category (category_id, category_name) \
        VALUES (1, 'category 1'), (2, 'category 2'), (3, 'category 3');")

        self.execute("INSERT INTO tag (tag_id, tag_name) \
        VALUES (1, 'tag 1'), (2, 'tag 2'), (3, 'tag 3'), (4, 'tag 4');")

        self.execute("INSERT INTO task (category_id, task_status, task_title, task_description) \
        VALUES \
        (1, 'p', 'task 1', 'description task 1'),\
        (2, 'p', 'task 2', 'description task 2 test test test test test test test test test test test test test test test test'),\
        (3, 'c', 'task 3', 'description test 3'),\
        (3, 'd', 'task 4', 'description test 4'),\
        (1, 'd', 'task 5', 'description test 5');")

        self.execute("INSERT INTO task_with_tag (task_id, tag_id) \
        VALUES (1, 1), (1, 2), (1, 3), (2, 2);")

    def create_tables(self):
        self.query('''
            CREATE TABLE category (
                category_id     INTEGER PRIMARY KEY AUTOINCREMENT,
                category_name   TEXT NOT NULL UNIQUE
            );
        ''')
        self.query('''
            CREATE TABLE tag (
                tag_id     INTEGER PRIMARY KEY AUTOINCREMENT,
                tag_name   TEXT NOT NULL UNIQUE
            );
        ''')
        # p = pending
        # c = completed
        # d = deferred
        self.query('''
            CREATE TABLE task (
                task_id             INTEGER PRIMARY KEY AUTOINCREMENT,
                task_status         CHAR(1) DEFAULT "p",
                task_title          TEXT    NOT NULL,
                task_description    TEXT,
                category_id         INTEGER,
                FOREIGN KEY(category_id) REFERENCES category(category_id) ON UPDATE CASCADE ON DELETE CASCADE
            );
        ''')
        self.query('''
            CREATE TABLE task_with_tag (
                this_id     INTEGER PRIMARY KEY AUTOINCREMENT,
                task_id     INTEGER NOT NULL,
                tag_id      INTEGER NOT NULL,
                FOREIGN KEY(task_id) REFERENCES task(task_id) ON UPDATE CASCADE ON DELETE CASCADE,
                FOREIGN KEY(tag_id) REFERENCES tag(tag_id) ON UPDATE CASCADE ON DELETE CASCADE
            );
        ''')

