from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton, QWidget, QVBoxLayout, QComboBox, QPlainTextEdit, QDialog, QMessageBox, \
    QHBoxLayout, QLabel, QLineEdit

from funs import sqlf
from Form_Category import Form_Category


class Form_EditTask(QDialog, QWidget):
    def __init__(self, parent=None, row=None):
        super(Form_EditTask, self).__init__(parent)
        self.title = 'Edit Task'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon("icons/icon.png"))
        self.setMinimumWidth(450)

        self.layout = QVBoxLayout(self)

        self.row = row

        # Title
        label_task_title = QLabel()
        label_task_title.setText('Task Title:')
        self.layout.addWidget(label_task_title)

        self.text_task_title = QLineEdit()
        self.text_task_title.setText(self.row["task_title"])
        self.layout.addWidget(self.text_task_title)

        box = QHBoxLayout()
        # Description
        label_task_description = QLabel()
        label_task_description.setText('Task Description (optional):')
        self.layout.addWidget(label_task_description)

        self.textarea_task_description = QPlainTextEdit()
        self.textarea_task_description.setPlainText(self.row["task_description"])
        self.layout.addWidget(self.textarea_task_description)

        self.comb_category = QComboBox()
        self.preparing_category_data()
        category_index = self.comb_category.findText(self.row["category_name"], Qt.MatchFixedString)
        if category_index >= 0:
            self.comb_category.setCurrentIndex(category_index)

        butt_category = QPushButton('Categories')
        butt_category.clicked.connect(self.click_category)
        butt_category.setMaximumWidth(100)

        box_category = QHBoxLayout()
        box_category.addWidget(self.comb_category)
        box_category.addWidget(butt_category)
        self.layout.addLayout(box_category)

        butt_editTask = QPushButton("Edit Task")
        butt_editTask.clicked.connect(self.click_editTask)
        self.layout.addWidget(butt_editTask)

    def click_category(self):
        form_category = Form_Category()
        form_category.exec_()
        self.preparing_category_data()

    def click_editTask(self):
        category_id = None
        if self.comb_category.count():
            category_id = self.comb_category.itemData(self.comb_category.currentIndex())
        else:
            return QMessageBox.about(self, "Alert", "No categories were found, please add categories")
        if not category_id:
            return QMessageBox.about(self, "Alert", "Please select a category")

        task_title = self.text_task_title.text()
        if not task_title:
            return QMessageBox.about(self, "Alert", "Please enter the task title")

        task_description = self.textarea_task_description.toPlainText()
        task_description = None if not task_description else task_description

        # Edit task
        status = sqlf.edit_task(self.row["task_id"], task_title, task_description, category_id)
        if status > 0:
            self.close()
        else:
            QMessageBox.about(self, "Error query", "An error in the process of input")

    def preparing_category_data(self):
        self.comb_category.clear()
        data = sqlf.get_all_category()
        if data:
            for row in data:
                self.comb_category.addItem(str(row["category_name"]), str(row["category_id"]))
