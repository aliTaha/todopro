# pyinstaller -w -F main.py
import os
import PyInstaller.__main__

PyInstaller.__main__.run([
    '--onefile',
    '--windowed',
    '--icon=%s' % os.path.join('I', r'I:\python\todopro2\icons', 'icon.ico'),
    os.path.join('AppMain.py'),
])