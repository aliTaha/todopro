class sqlf():
    # public static var class "sqlclass"
    conf = None

    #####################################################################
    @staticmethod
    def convert_status(status):
        if status == "p":
            return "pending"
        if status == "c":
            return "completed"
        if status == "d":
            return "deferred"
    #####################################################################
    # methods table category

    @staticmethod
    def get_all_category():
        return sqlf.conf.cur_fetchall("SELECT * FROM category;")

    @staticmethod
    def get_category_by_name(name):
        return sqlf.conf.cur_fetchone("SELECT * FROM `category` WHERE `category_name`=?;", name)

    @staticmethod
    def add_category(name):
        return sqlf.conf.execute('INSERT INTO category(category_name) VALUES(?);', name)

    @staticmethod
    def edit_category(id, name):
        return sqlf.conf.execute('UPDATE `category` SET `category_name`=? WHERE `category_id`=?;',name, id)

    @staticmethod
    def delete_category_by_id(category_id):
        return sqlf.conf.execute('DELETE FROM category WHERE category_id=?;', category_id)

    #####################################################################
    # methods table tag

    @staticmethod
    def get_all_tag():
        return sqlf.conf.cur_fetchall("SELECT * FROM tag;")

    @staticmethod
    def get_tag_by_name(name):
        return sqlf.conf.cur_fetchone("SELECT * FROM `tag` WHERE `tag_name`=?;", name)

    @staticmethod
    def add_tag(name):
        return sqlf.conf.execute('INSERT INTO tag(tag_name) VALUES(?);', name)

    @staticmethod
    def delete_tag_by_id(tag_id):
        return sqlf.conf.execute('DELETE FROM tag WHERE tag_id=?;', tag_id)

    #####################################################################
    # methods table task

    @staticmethod
    def add_task(title, description, status, category_id):
        return sqlf.conf.cur_insert('INSERT INTO `task`(`task_title`, `task_description`, `task_status`, `category_id`) VALUES(?, ?, ?, ?)',
                                 title, description, status, category_id)

    @staticmethod
    def edit_task(id, title, description, category_id):
        return sqlf.conf.execute('UPDATE `task` SET `task_title`=?, `task_description`=?, `category_id`=? WHERE `task_id`=?;',
                                 title, description, category_id, id)

    @staticmethod
    def edit_task_status(id, status):
        return sqlf.conf.execute('UPDATE `task` SET `task_status`=? WHERE `task_id`=?;', status, id)

    @staticmethod
    def get_all_task():
        return sqlf.conf.cur_fetchall(
            "SELECT *, (SELECT category_name FROM category WHERE category.category_id=task.category_id) AS `category_name` FROM task;")

    @staticmethod
    def get_all_task_by_category_id(category_id):
        return sqlf.conf.cur_fetchall(
            "SELECT *, (SELECT category_name FROM category WHERE category.category_id=task.category_id) AS `category_name` FROM task WHERE `category_id`=?;", category_id)

    @staticmethod
    def get_all_task_pending():
        return sqlf.conf.cur_fetchall(
            "SELECT *, (SELECT category_name FROM category WHERE category.category_id=task.category_id) AS `category_name` FROM task WHERE task_status='p';")

    @staticmethod
    def get_all_task_completed():
        return sqlf.conf.cur_fetchall(
            "SELECT *, (SELECT category_name FROM category WHERE category.category_id=task.category_id) AS `category_name` FROM task WHERE task_status='c';")

    @staticmethod
    def get_all_task_deferred():
        return sqlf.conf.cur_fetchall(
            "SELECT *, (SELECT category_name FROM category WHERE category.category_id=task.category_id) AS `category_name` FROM task WHERE task_status='d';")

    @staticmethod
    def delete_task_by_id(id):
        return sqlf.conf.execute('DELETE FROM `task` WHERE `task_id`=?', id)

    #####################################################################
    # methods table task_with_tag

    @staticmethod
    def add_task_with_tag(task_id, tag_id):
        return sqlf.conf.execute('INSERT INTO `task_with_tag`(task_id, tag_id) VALUES(?, ?);', task_id, tag_id)

    @staticmethod
    def get_tags_by_task_id(task_id):
        return sqlf.conf.cur_fetchall(
            "SELECT *, (SELECT `tag_name` FROM `tag` WHERE `tag`.tag_id=`task_with_tag`.tag_id) AS `tag_name` FROM `task_with_tag` WHERE `task_id`=?;", task_id)

