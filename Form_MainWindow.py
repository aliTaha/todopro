from PyQt5.QtWidgets import QMainWindow, QPushButton, QWidget, QTabWidget, QVBoxLayout, QApplication
from PyQt5.QtGui import QIcon

from TabTableTasks import TabTableTasks
from Form_NewTask import Form_NewTask

class Form_MainWindow(QMainWindow):

    def __init__(self, parent=None):
        super(Form_MainWindow, self).__init__(parent)

        self.title = 'ProToDo'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon("icons/icon.png"))
        self.left = 0
        self.top = 0
        self.width = 850
        self.height = 600

        self.setGeometry(self.left, self.top, self.width, self.height)
        self.center()

        _widget = QWidget(self)
        self.layout = QVBoxLayout(_widget)
        self.setCentralWidget(_widget)

        # Initialize tab screen
        self.tabs = QTabWidget()

        # Add tabs
        self.tab_general = TabTableTasks(self, 'general')
        self.tabs.addTab(self.tab_general, "General Tasks")

        self.tab_pending = TabTableTasks(self, 'pending')
        self.tabs.addTab(self.tab_pending, "Pending Tasks")

        self.tab_completed = TabTableTasks(self, 'completed')
        self.tabs.addTab(self.tab_completed, "Completed Tasks")

        self.tab_deferred = TabTableTasks(self, 'deferred')
        self.tabs.addTab(self.tab_deferred, "Deferred Tasks")

        # Add tabs to widget
        self.layout.addWidget(self.tabs)

        butt_newTask = QPushButton('New Task')
        butt_newTask.clicked.connect(self.click_newTask)
        self.layout.addWidget(butt_newTask)

    def click_newTask(self):
        self.form_NewTask = Form_NewTask()
        self.form_NewTask.exec_()
        self.tab_general.relocadData()
        self.tab_pending.relocadData()
        self.tab_completed.relocadData()
        self.tab_deferred.relocadData()
        # self.form_NewTask.activateWindow() # or self.dialog.raise_()

    def center(self):
        frameGm = self.frameGeometry()
        screen = QApplication.desktop().screenNumber(QApplication.desktop().cursor().pos())
        centerPoint = QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())