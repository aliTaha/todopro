from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton, QWidget, QVBoxLayout, QLineEdit, QMessageBox, QDialog, QLabel, QTableWidget, \
    QHeaderView, QHBoxLayout, QGroupBox, QGridLayout

from funs import sqlf

class Form_Tag(QDialog, QWidget):
    def __init__(self, parent=None):
        super(Form_Tag, self).__init__(parent)
        self.setupUi(self)

    def setupUi(self, Dialog):
        self.title = 'Tags'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon("icons/icon.png"))

        self.layout = QVBoxLayout(self)
        Dialog.resize(340, 320)

        self.grid = QGridLayout()
        self.layout.addLayout(self.grid)

        self.setupUi_add()
        self.setupUi_edit()

        # Tag Table
        self.MainTable = QTableWidget()
        self.MainTable.setShowGrid(True)
        self.MainTable.setColumnCount(2)
        self.MainTable.setHorizontalHeaderLabels(['Tag', 'Actions'])
        self.MainTable.resize(240, 320)
        self.MainTable.verticalHeader().setDefaultSectionSize(36)

        box_tags = QHBoxLayout()
        box_tags.addWidget(self.MainTable)
        self.layout.addLayout(box_tags)

        header = self.MainTable.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)

        self.loadData()

    def setupUi_add(self):
        self.groupBox_add = QGroupBox("Add Tag")
        self.grid.addWidget(self.groupBox_add, 0, 0)
        vbox = QVBoxLayout()
        self.groupBox_add.setLayout(vbox)

        label_tag_name = QLabel()
        label_tag_name.setText('Tag Name:')
        vbox.addWidget(label_tag_name)

        self.groupAdd_text_tag_name = QLineEdit()
        vbox.addWidget(self.groupAdd_text_tag_name)

        butt_addTag = QPushButton('Add Tag')
        butt_addTag.clicked.connect(self.click_addTag)
        vbox.addWidget(butt_addTag)

    def setupUi_edit(self):
        self.groupBox_edit = QGroupBox("Edit Tag")
        self.grid.addWidget(self.groupBox_edit, 0, 1)
        vbox = QVBoxLayout()
        self.groupBox_edit.setLayout(vbox)

        label_tag_name = QLabel()
        label_tag_name.setText('Tag Name:')
        vbox.addWidget(label_tag_name)

        self.groupEdit_text_tag_name = QLineEdit()
        vbox.addWidget(self.groupEdit_text_tag_name)

        butt_editTag = QPushButton('Save')
        butt_editTag.clicked.connect(self.click_editTag)
        vbox.addWidget(butt_editTag)

        butt_cansel = QPushButton('Cansel')
        butt_cansel.setMaximumWidth(70)
        butt_cansel.clicked.connect(self.click_butt_cansel)
        vbox.addWidget(butt_cansel)

        box_buttons = QHBoxLayout()
        box_buttons.addWidget(butt_editTag)
        box_buttons.addWidget(butt_cansel)
        vbox.addLayout(box_buttons)

        self.groupBox_edit.setVisible(False)

    def click_butt_cansel(self):
        self.groupBox_edit.setVisible(False)
        self.groupBox_add.setVisible(True)

    def click_editTag(self):

        tag_id = self.edit_tag_id
        tag_name = self.groupEdit_text_tag_name.text()
        if not tag_name:
            return QMessageBox.about(self, "Alert", "Please enter the tag name")

        tag = sqlf.get_tag_by_name(tag_name)
        if tag and tag["tag_id"] != tag_id:
            return QMessageBox.about(self, "Error", "The tag name already exists")

        status = sqlf.edit_tag(tag_id, tag_name)
        if status > 0:
            self.groupBox_add.setVisible(True)
            self.groupBox_edit.setVisible(False)
            self.loadData()
        else:
            QMessageBox.about(self, "Error query", "An error in the process of input")

    def click_addTag(self):
        tag_name = self.groupAdd_text_tag_name.text()
        if not tag_name:
            return QMessageBox.about(self, "Alert", "Please enter the tag name")

        tag = sqlf.get_tag_by_name(tag_name)
        if tag:
            return QMessageBox.about(self, "Error", "The tag name already exists")

        status = sqlf.add_tag(tag_name)
        if status > 0:
            self.groupAdd_text_tag_name.clear()
            self.loadData()
        else:
            QMessageBox.about(self, "Error query", "An error in the process of input")

    def loadData(self):
        # clear all data in table
        self.MainTable.setRowCount(0)

        results = sqlf.get_all_tag()
        if results:
            for row in results:
                self.fetch_row(row)

    def fetch_row(self, row):
        rowPosition = self.MainTable.rowCount()
        self.MainTable.insertRow(rowPosition)

        class StyleTitle(QWidget):
            def __init__(self, parent=None):
                super(StyleTitle, self).__init__(parent)
                self.parent = parent

                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignCenter | Qt.AlignLeft)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                label = QLabel()
                label.setText(row["tag_name"])
                layout.addWidget(label)

                self.setLayout(layout)

        self.MainTable.setCellWidget(rowPosition, 0, StyleTitle(self))

        class StyleButtonsActions(QWidget):

            def __init__(self, parent=None):
                super(StyleButtonsActions, self).__init__(parent)
                self.parent = parent
                # add your buttons
                layout = QHBoxLayout()
                layout.setAlignment(Qt.AlignCenter)
                layout.setContentsMargins(5, 5, 5, 5)
                layout.setSpacing(0)

                ### button edit
                butt_edit = QPushButton()
                butt_edit.clicked.connect(self.click_butt_edit)
                butt_edit.setIcon(QIcon("icons/edit.png"))
                layout.addWidget(butt_edit)

                ### button delete
                butt_delete = QPushButton()
                # butt_delete.row = row
                butt_delete.clicked.connect(self.click_butt_delete)
                butt_delete.setIcon(QIcon("icons/delete.png"))
                layout.addWidget(butt_delete)

                self.setLayout(layout)

            def click_butt_edit(self):
                self.parent.groupBox_add.setVisible(False)
                self.parent.groupBox_edit.setVisible(True)

                self.parent.edit_tag_id = row["tag_id"]
                self.parent.groupEdit_text_tag_name.setText(row["tag_name"])

            def click_butt_delete(self):
                tag_id = row["tag_id"]
                tag_name = row["tag_name"]

                if sqlf.get_all_task_by_tag_id(tag_id):
                    return QMessageBox.about(self, "Warning", "The ({}) tag cannot be deleted because it contains tasks, please delete the associated tasks first".format(tag_name))

                msgBox = QMessageBox(self)
                reply = msgBox.question(self, 'Warning',("Are you sure to delete:({})".format(tag_name)),
                                        QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    if sqlf.delete_tag_by_id(tag_id) < 1:
                        return QMessageBox.about(self, "Error query", "The tag:({}) was not deleted".format(tag_name))

                self.parent.loadData()

        self.MainTable.setCellWidget(rowPosition, 1, StyleButtonsActions(self))
