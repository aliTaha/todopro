from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QPushButton, QWidget, QVBoxLayout, QComboBox, QPlainTextEdit, QDialog, QMessageBox, \
    QLabel, QLineEdit, QHBoxLayout, QListWidget, QFormLayout, QListWidgetItem

from Form_Tag import Form_Tag
from funs import sqlf
from Form_Category import Form_Category


class Form_NewTask(QDialog, QWidget):
    def __init__(self, parent=None):
        super(Form_NewTask, self).__init__(parent)

        self.title = 'Add New Task'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon("icons/icon.png"))
        self.setMinimumWidth(450)

        self.layout = QVBoxLayout(self)

        # Status
        label_status = QLabel('Status:')
        self.layout.addWidget(label_status)

        self.comb_type = QComboBox()
        self.comb_type.addItem("Pending", 'p')
        self.comb_type.addItem("Completed", 'c')
        self.comb_type.addItem("Deferred", 'd')
        self.layout.addWidget(self.comb_type)

        # Title
        label_task_title = QLabel('Task Title:')
        self.layout.addWidget(label_task_title)

        self.text_task_title = QLineEdit()
        self.layout.addWidget(self.text_task_title)

        # Description
        label_task_description = QLabel('Task Description (optional):')
        self.layout.addWidget(label_task_description)

        self.textarea_task_description = QPlainTextEdit()
        self.layout.addWidget(self.textarea_task_description)

        # Category
        self.comb_category = QComboBox()
        self.preparing_category_data()

        butt_category = QPushButton('Categories')
        butt_category.clicked.connect(self.click_category)
        butt_category.setMaximumWidth(100)

        box_category = QHBoxLayout()
        box_category.addWidget(self.comb_category)
        box_category.addWidget(butt_category)
        self.layout.addLayout(box_category)

        # Tags
        layout_tags = QFormLayout()

        self.list_tags = QListWidget()
        self.list_tags.doubleClicked.connect(self.doubleClicked_list_tags)
        self.preparing_tags_data()

        self.list_tags_task = QListWidget()
        self.list_tags_task.doubleClicked.connect(self.doubleClicked_list_tags_task)

        layout_tags.addRow(QLabel('Tags task (optional):'), QLabel('Tags:'))
        layout_tags.addRow(self.list_tags_task, self.list_tags)

        butt_tag = QPushButton('Tags')
        butt_tag.clicked.connect(self.click_tag)
        layout_tags.addRow(butt_tag)

        self.layout.addLayout(layout_tags)

        # Add Task
        butt_addTask = QPushButton('Add Task')
        butt_addTask.clicked.connect(self.click_addTask)
        self.layout.addWidget(butt_addTask)

    def doubleClicked_list_tags(self, QModelIndex):
        item_ListWidgetItem = self.list_tags.itemFromIndex(QModelIndex)
        item = self.list_tags.row(item_ListWidgetItem)
        self.list_tags.takeItem(item)
        self.list_tags_task.addItem(item_ListWidgetItem)

    def doubleClicked_list_tags_task(self, QModelIndex):
        item_ListWidgetItem = self.list_tags_task.itemFromIndex(QModelIndex)
        item = self.list_tags_task.row(item_ListWidgetItem)
        self.list_tags_task.takeItem(item)
        self.list_tags.addItem(item_ListWidgetItem)

    def click_category(self):
        form_category = Form_Category()
        form_category.exec_()
        self.preparing_category_data()

    def click_tag(self):
        form_tag = Form_Tag()
        form_tag.exec_()
        self.preparing_tags_data()

    def preparing_tags_data(self):
        self.list_tags.clear()
        data = sqlf.get_all_tag()
        if data:
            for row in data:
                item = QListWidgetItem()
                item.setData(Qt.UserRole, row["tag_id"])
                item.setText(row["tag_name"])
                self.list_tags.addItem(item)

    def click_addTask(self):

        if self.comb_type.count():
            task_status = self.comb_type.itemData(self.comb_type.currentIndex())
        else:
            return QMessageBox.about(self, "Alert", "No status was found")
        if not task_status:
            return QMessageBox.about(self, "Alert", "Please select a status")

        category_id = None
        if self.comb_category.count():
            if self.comb_category.currentIndex() > 0 :
                category_id = self.comb_category.itemData(self.comb_category.currentIndex())

        task_title = self.text_task_title.text()
        if not task_title:
            return QMessageBox.about(self, "Alert", "Please enter the task title")

        task_description = self.textarea_task_description.toPlainText()
        task_description = None if not task_description else task_description

        # add task data
        task_id = sqlf.add_task(task_title, task_description,  task_status, category_id)
        if task_id:
            for i in range(self.list_tags_task.count()):
                tag_id = self.list_tags_task.item(i).data(Qt.UserRole)
                sqlf.add_task_with_tag(task_id, tag_id)

            self.text_task_title.clear()
            self.textarea_task_description.clear()
            self.list_tags_task.clear()
            self.preparing_tags_data()
            # self.close()
        else:
            QMessageBox.about(self, "Error query", "An error in the process of input")

    def preparing_category_data(self):
        self.comb_category.clear()
        self.comb_category.addItem("--- Categories ---")
        data = sqlf.get_all_category()
        if data:
            for row in data:
                self.comb_category.addItem(str(row["category_name"]), str(row["category_id"]))
